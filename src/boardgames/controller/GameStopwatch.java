package boardgames.controller;

import javax.swing.Timer;

import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.TimeUnit;

import boardgames.view.game.TimeGameViewImpl;

/**
 * Class that implements the stopwatches for 2 players. It used to determinate
 * who wins before the time is up
 *
 */
public final class GameStopwatch {

    private static Timer timerW;
    private static Timer timerB;
    private static Timer timerP;
    private static StopWatch wStopwatch;
    private static StopWatch bStopwatch;
    private static StopWatch playTime;
    private static final Integer MAXMINUTES = 15;
    private static final String MS = "mm:ss";
    private static final String HMS = "HH:mm:ss";
    private static final Logger LOGGER = LogManager.getLogger(GameStopwatch.class.getName());

    /**
     * Method that create white stopwatch and execute it every second in a new
     * thread with a Timer that starts itself.
     */
    public void wStart() {

        wStopwatch = StopWatch.createStarted();
        timerW = new Timer(1000, e -> {

            if (isTimeFinished(wStopwatch)) {
                LOGGER.trace("White timer is finished");
                timerW.stop();
                TimeGameViewImpl.timedVictoryPanel();
            }
            TimeGameViewImpl.updateWhiteTimer(DurationFormatUtils.formatDuration(wStopwatch.getTime(), MS));
        });

        timerW.setInitialDelay(0);
        timerW.start();
    }

    /**
     * Method that create black stopwatch and execute it every second in a new
     * thread with a Timer that starts itself.
     */
    public void bStart() {

        bStopwatch = StopWatch.createStarted();
        timerB = new Timer(1000, e -> {

            if (isTimeFinished(bStopwatch)) {
                LOGGER.trace("Black timer is finished");
                timerB.stop();
                TimeGameViewImpl.timedVictoryPanel();
            }
            TimeGameViewImpl.updateBlackTimer(DurationFormatUtils.formatDuration(bStopwatch.getTime(), MS));
        });
        timerB.setInitialDelay(0);
        timerB.start();
    }

    /**
     * Method that create the play time stopwatch and execute it every second in a
     * new thread with a Timer that starts itself.
     */
    public void playTimeStart() {
        playTime = StopWatch.createStarted();
        timerP = new Timer(1000, e -> TimeGameViewImpl
                .updatePlayTime(DurationFormatUtils.formatDuration(playTime.getTime(), HMS)));
        timerP.setInitialDelay(0);
        timerP.start();
    }

    /**
     * @return the white player stopwatch
     */
    public static StopWatch getWstopwatch() {
        return wStopwatch;
    }

    /**
     * @return the black player stopwatch
     */
    public static StopWatch getBstopwatch() {
        return bStopwatch;
    }

    /**
     * @return the current time of playtime stopwatch
     */
    public static StopWatch getPlayTime() {
        LOGGER.trace(" Getting how long the game lasted ");
        return playTime;
    }

    /**
     * Method that stops the play time timer.
     */
    public static void stopPlayTimer() {
        timerP.stop();
    }

    /**
     * @param s
     *            the stopwatch from which to evaluate the time
     * @return true if maxMinutes minutes have passed
     */
    private static boolean isTimeFinished(final StopWatch s) {
        return s.getTime(TimeUnit.MINUTES) == MAXMINUTES;
    }


}
