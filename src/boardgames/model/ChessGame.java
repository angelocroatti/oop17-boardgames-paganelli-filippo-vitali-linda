package boardgames.model;

import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import boardgames.model.board.Box;
import boardgames.model.board.ChessBoard;
import boardgames.model.piece.Bishop;
import boardgames.model.piece.Knight;
import boardgames.model.piece.PieceImpl;
import boardgames.model.piece.Queen;
import boardgames.model.piece.Rook;
import boardgames.utility.AllChessPieces;
import boardgames.utility.Colour;
import boardgames.view.game.GameView;

/**
 * This class implements the abstract functions defined by GameImpl useful for establishing the logic of chess.
 */
public class ChessGame extends GameImpl {

    private final CheckMateImpl kingW;
    private final CheckMateImpl kingB;
    private static final Logger LOGGER = LogManager.getLogger(ChessGame.class.getName());

    /**
     * Sets up chessboard, players and king's checkers useful for chess.
     */
    public ChessGame() {
        super();
        final ChessBoard cb = new ChessBoard();
        this.getBoard().setBoardType(cb);
        this.getBoard().reset();
        this.setPlayer(Colour.White, new PlayerImpl(Colour.White, this.getBoard()));
        this.setPlayer(Colour.Black, new PlayerImpl(Colour.Black, this.getBoard()));
        this.kingW = new CheckMateImpl(this, cb.getWhiteKing(), this.getPlayer(Colour.White));
        this.kingB = new CheckMateImpl(this, cb.getBlackKing(), this.getPlayer(Colour.Black));
    }

    @Override
    public final void checkPromotion(final PieceImpl pieceToMove, final Box newPosition) {
        if (pieceToMove.getName().equals(AllChessPieces.Pawn.toString())) {
            if (pieceToMove.getColour() == Colour.White && newPosition.getY() == UPPER_BOUND) {
                this.notifyPromotion();
            } else if (pieceToMove.getColour() == Colour.Black && newPosition.getY() == LOWER_BOUND) {
                this.notifyPromotion();
            }
        }
    }

    @Override
    public final boolean checkEat(final Box newPosition, final PieceImpl pieceToMove) {
    if ((!newPosition.isEmpty()) && (newPosition.getPiece().get().getColour() != pieceToMove.getColour())) {
        this.eat(pieceToMove, newPosition.getPiece().get());
        return true;
        } else {
            return false;
        }
    }

    @Override
    public final void promotion(final Colour player, final String pieceUpgraded) {
        try {
            switch (pieceUpgraded) {
                case "Queen" : this.getLastPieceMoved().get().setPieceType(new Queen());
                               break;
                case "Rook"  : this.getLastPieceMoved().get().setPieceType(new Rook());
                               break;
                case "Knight": this.getLastPieceMoved().get().setPieceType(new Knight());
                               break;
                case "Bishop": this.getLastPieceMoved().get().setPieceType(new Bishop());
                               break;
                default : LOGGER.debug("promotion without success");
            }
            this.checkWinner(player);
        } catch (NoSuchElementException e) {
            LOGGER.debug("no piece has yet been moved");
        }
    }

    @Override
    public final boolean checkWinner(final Colour player) {
        try {
            if (player == Colour.White) {
                return this.kingB.isCheckMate(this.getLastPieceMoved().get());
            } else {
                return this.kingW.isCheckMate(this.getLastPieceMoved().get());
            }
        } catch (NoSuchElementException e) {
            LOGGER.debug("no piece has yet been moved");
        }
        return false;
    }

    @Override
    public final void attach(final GameView gv) {
        super.attach(gv);
        this.kingB.attach(gv);
        this.kingW.attach(gv);
    }

    /**
     * @return a CheckMateImpl that is the white king status controller
     */
    public final CheckMateImpl getWhiteKingCheckMateController() {
        return this.kingW;
    }

    /**
     * @return a CheckMateImpl that is the black king status controller
     */
    public final CheckMateImpl getBlackKingCheckMateController() {
        return this.kingB;
    }

    @Override
    public final boolean checkMove(final Box newPosition, final PieceImpl pieceToMove) {
        final List<Box> opponentsMoves = new LinkedList<>();
        boolean canMove = false;
        if (pieceToMove.possibleMoves(this.getBoard()).contains(newPosition)) {
            if (!pieceToMove.getName().equals(AllChessPieces.King.toString())) {
                final Box from = pieceToMove.getBox().get();
                if (newPosition.isEmpty()) {
                    // simulo mossa
                    newPosition.setPiece(Optional.of(pieceToMove));
                    pieceToMove.setBox(Optional.of(newPosition));
                    from.setPiece(Optional.empty());
                    if (pieceToMove.getColour().equals(Colour.Black)) {
                        this.getPlayer(Colour.White).getPlayerPieces().forEach(piece -> {
                            if (piece.getBox().isPresent()) {
                                opponentsMoves.addAll(piece.possibleMoves(this.getBoard()));
                            }
                        });
                        if (!opponentsMoves.contains(this.kingB.getKing().getBox().get())) {
                            canMove = true;
                        }
                    } else {
                        this.getPlayer(Colour.Black).getPlayerPieces().forEach(piece -> {
                            if (piece.getBox().isPresent()) {
                                opponentsMoves.addAll(piece.possibleMoves(this.getBoard()));
                            }
                        });
                        if (!opponentsMoves.contains(this.kingW.getKing().getBox().get())) {
                            canMove = true;
                        }
                    }
                    from.setPiece(Optional.of(pieceToMove));
                    pieceToMove.setBox(Optional.of(from));
                    newPosition.setPiece(Optional.empty());
                } else {
                    // simulo mangiata
                    final PieceImpl temp = newPosition.getPiece().get();
                    temp.setBox(Optional.empty());
                    newPosition.setPiece(Optional.of(pieceToMove));
                    pieceToMove.setBox(Optional.of(newPosition));
                    from.setPiece(Optional.empty());
                    if (pieceToMove.getColour().equals(Colour.Black)) {
                        this.getPlayer(Colour.White).removePiece(temp);
                        this.getPlayer(Colour.White).getPlayerPieces().forEach(piece -> {
                            if (!piece.equals(temp)) {
                                opponentsMoves.addAll(piece.possibleMoves(this.getBoard()));
                            }
                        });
                        if (!opponentsMoves.contains(this.kingB.getKing().getBox().get())) {
                            canMove = true;
                        }
                        this.getPlayer(Colour.White).addPiece(temp);
                    } else {
                        this.getPlayer(Colour.Black).removePiece(temp);
                        this.getPlayer(Colour.Black).getPlayerPieces().forEach(piece -> {
                            if (!piece.equals(temp)) {
                                opponentsMoves.addAll(piece.possibleMoves(this.getBoard()));
                            }
                        });
                        if (!opponentsMoves.contains(this.kingW.getKing().getBox().get())) {
                            canMove = true;
                        }
                        this.getPlayer(Colour.Black).addPiece(temp);
                    }
                    temp.setBox(Optional.of(newPosition));
                    from.setPiece(Optional.of(pieceToMove));
                    pieceToMove.setBox(Optional.of(from));
                    newPosition.setPiece(Optional.of(temp));
                }
                if (!canMove) {
                    final String log = "Can't move the " + pieceToMove.getName() + " in Box " + newPosition.toString() + " because king can be eated";
                    this.notifyConsole(log);
                    LOGGER.debug(log);
                }
            } else {
                canMove = true;
            }
        } else {
            final String log = "Can't move the " + pieceToMove.getName() + " in Box " + newPosition.toString();
                this.notifyConsole(log);
                LOGGER.debug(log);
        }
        return canMove;
    }

    @Override
    public final void pieceRemover(final PieceImpl pieceEaten) {
        if (pieceEaten.getColour().equals(Colour.Black)) {
          this.getBoard().removeBlackPiece(pieceEaten);
      } else {
          this.getBoard().removeWhitePiece(pieceEaten);
      }
    }
}
