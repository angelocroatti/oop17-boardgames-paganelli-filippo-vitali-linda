package boardgames.model;

import java.util.List;
import boardgames.model.piece.PieceImpl;
import boardgames.utility.Colour;

/**
 * This interface defines the functions useful for modeling the player.
 */
public interface Player {

    /**
     * @return the List containing the Player's Pieces
     */
    List<PieceImpl> getPlayerPieces();

    /**
     * Removes the past Piece from the Player's pieces if contains it.
     * @param p Piece to be removed from the Player's Pieces
     */
    void removePiece(PieceImpl p);

    /**
     * Add the past piece to the Player's Pieces if they have the same colour.
     * @param p Piece to be added to the Player's Pieces
     */
    void addPiece(PieceImpl p);

    /**
     * @return the Colour of the Player
     */
    Colour getColour();
}
