package boardgames.model.board;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;


import boardgames.model.piece.PieceImpl;
import boardgames.model.piece.CheckersPawn;
import boardgames.utility.Colour;
import boardgames.utility.PairImpl;
import boardgames.utility.PieceUtilsImpl;

/**
 * This class implements the function for modeling the Checkers board.
 */
public class CheckersBoard implements BoardType {

    private static final int FIRST_WHITE_ROW = 0;
    private static final int SECOND_WHITE_ROW = 1;
    private static final int THIRD_WHITE_ROW = 2;
    private static final int FIRST_BLACK_ROW = 7;
    private static final int SECOND_BLACK_ROW = 6;
    private static final int THIRD_BLACK_ROW = 5;

    private final List<PieceImpl> whitePieces;
    private final List<PieceImpl> blackPieces;
    private final List<List<Box>> allPiecesOnBoard;

    /**
     * Initializes the board and the pieces that use it.
     */
    public CheckersBoard() {
        this.whitePieces = new LinkedList<>();
        this.blackPieces = new LinkedList<>();
        this.allPiecesOnBoard = new LinkedList<>();

        for (int x = 0; x <= PieceUtilsImpl.BOARD_SIZE; x++) {
            this.allPiecesOnBoard.add(new LinkedList<Box>());
            for (int y = 0; y <= PieceUtilsImpl.BOARD_SIZE; y++) {
                this.allPiecesOnBoard.get(x).add(new Box(new PairImpl<>(x, y), Optional.empty()));
            }
        }
    }

    @Override
    public final List<List<Box>> reset() {
            for (int i = 1; i <= PieceUtilsImpl.BOARD_SIZE; i += 2) {
                // initialization of first white row
                final PieceImpl cp0 = new PieceImpl(allPiecesOnBoard.get(i).get(FIRST_WHITE_ROW), Colour.White);
                cp0.setPieceType(new CheckersPawn());
                this.addWhitePiece(cp0);
                allPiecesOnBoard.get(i).get(FIRST_WHITE_ROW).setPiece(Optional.of(cp0));
                // initialization of third white row
                final PieceImpl cp2 = new PieceImpl(this.allPiecesOnBoard.get(i).get(THIRD_WHITE_ROW), Colour.White);
                cp2.setPieceType(new CheckersPawn());
                this.addWhitePiece(cp2);
                allPiecesOnBoard.get(i).get(THIRD_WHITE_ROW).setPiece(Optional.of(cp2));
                // initialization of second black row
                final PieceImpl cp6 = new PieceImpl(allPiecesOnBoard.get(i).get(SECOND_BLACK_ROW), Colour.Black);
                this.addBlackPiece(cp6);
                cp6.setPieceType(new CheckersPawn());
                allPiecesOnBoard.get(i).get(SECOND_BLACK_ROW).setPiece(Optional.of(cp6));
            }
            for (int c = 0; c < PieceUtilsImpl.BOARD_SIZE; c += 2) {
                // initialization of second white row
                final PieceImpl cp1 = new PieceImpl(allPiecesOnBoard.get(c).get(SECOND_WHITE_ROW), Colour.White);
                cp1.setPieceType(new CheckersPawn());
                this.addWhitePiece(cp1);
                allPiecesOnBoard.get(c).get(SECOND_WHITE_ROW).setPiece(Optional.of(cp1));
                // initialization of first black row
                final PieceImpl cp7 = new PieceImpl(allPiecesOnBoard.get(c).get(FIRST_BLACK_ROW), Colour.Black);
                cp7.setPieceType(new CheckersPawn());
                this.addBlackPiece(cp7);
                allPiecesOnBoard.get(c).get(FIRST_BLACK_ROW).setPiece(Optional.of(cp7));
                // initialization of third black row
                final PieceImpl cp5 = new PieceImpl(allPiecesOnBoard.get(c).get(THIRD_BLACK_ROW), Colour.Black);
                cp5.setPieceType(new CheckersPawn());
                this.addBlackPiece(cp5);
                allPiecesOnBoard.get(c).get(THIRD_BLACK_ROW).setPiece(Optional.of(cp5));
            }

        return this.allPiecesOnBoard;
    }

    private void addWhitePiece(final PieceImpl wp) {
        this.whitePieces.add(wp);
    }

    private void addBlackPiece(final PieceImpl bp) {
        this.blackPieces.add(bp);
    }

    @Override
    public final List<PieceImpl> getWhiteStartPieces() {
        return this.whitePieces;
    }

    @Override
    public final List<PieceImpl> getBlackStartPieces() {
        return this.blackPieces;
    }

}
