package boardgames.model.piece;

import java.util.LinkedList;
import java.util.List;
import boardgames.model.board.BoardImpl;
import boardgames.model.board.Box;
import boardgames.utility.Colour;
import boardgames.utility.PieceUtilsImpl;

/**
 *  This class implements the functions defined by PieceType for representing the Knight's logic.
 */
public class Knight implements PieceType {

    private final PieceUtilsImpl pu = new PieceUtilsImpl();
    private static final String NAME = "Knight";

    @Override
    public final List<Box> possibleMoves(final BoardImpl b, final Box current, final Colour colour) {
        final int x = current.getX();
        final int y = current.getY();
        final List<Box> moves = new LinkedList<>();

        if (this.pu.checkRange(x + 2, y - 1) 
                && this.pu.checkOpponent(x + 2, y - 1, colour, b))  {
            moves.add(b.getBox(x + 2, y - 1).get());
        }
        if (this.pu.checkRange(x + 2, y + 1)
                && this.pu.checkOpponent(x + 2, y + 1, colour, b)) {
            moves.add(b.getBox(x + 2, y + 1).get());
        }
        if (this.pu.checkRange(x + 1, y + 2)
                && this.pu.checkOpponent(x + 1, y + 2, colour, b)) {
            moves.add(b.getBox(x + 1, y + 2).get());
        }
        if (this.pu.checkRange(x - 1, y + 2)
                && this.pu.checkOpponent(x - 1, y + 2, colour, b)) {
            moves.add(b.getBox(x - 1, y + 2).get());
        }
        if (this.pu.checkRange(x - 2, y + 1)
                && this.pu.checkOpponent(x - 2, y + 1, colour, b)) {
            moves.add(b.getBox(x - 2, y + 1).get());
        }
        if (this.pu.checkRange(x - 2, y - 1)
                && this.pu.checkOpponent(x - 2, y - 1, colour, b)) {
            moves.add(b.getBox(x - 2, y - 1).get());
        }
        if (this.pu.checkRange(x + 1, y - 2)
                && this.pu.checkOpponent(x + 1, y - 2, colour, b)) {
            moves.add(b.getBox(x + 1, y - 2).get());
        }
        if (this.pu.checkRange(x - 1, y - 2)
                && this.pu.checkOpponent(x - 1, y - 2, colour, b)) {
            moves.add(b.getBox(x - 1, y - 2).get());
        }
        return moves;
    }

    @Override
    public final String getName() {
        return NAME;
    }
}
