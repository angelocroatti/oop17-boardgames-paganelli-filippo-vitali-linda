package boardgames.model.piece;

import java.util.LinkedList;
import java.util.List;
import boardgames.model.board.BoardImpl;
import boardgames.model.board.Box;
import boardgames.utility.Colour;
import boardgames.utility.PieceUtilsImpl;

/**
 * This class implements the functions defined by PieceType for representing the Pawn's logic.
 */
public class Pawn implements PieceType {

    private boolean isFirstMove = true;
    private final Box initial;
    private final PieceUtilsImpl pu = new PieceUtilsImpl();
    private static final  String NAME = "Pawn";

    /**
     * @param piecePosition the box where the piece is initially located
     */
    public Pawn(final Box piecePosition) {
        this.initial = piecePosition;
    }

    @Override
    public final List<Box> possibleMoves(final BoardImpl b, final Box current, final Colour colour) {
        final int x = current.getX();
        final int y =  current.getY();
        final List<Box> moves = new LinkedList<>();
        if (this.initial != current) {
            this.isFirstMove = false;
        } else {
            this.isFirstMove = true;
        }
        if (colour == Colour.White) {
            if ((isFirstMove) 
                    && (b.getBox(x, y + 2).get().isEmpty() && b.getBox(x, y + 1).get().isEmpty())) {
                        moves.add(b.getBox(x, y + 2).get());
            }

            if ((this.pu.checkRange(x, y + 1)) 
                    && (b.getBox(x, y + 1).get().isEmpty())) {
                        moves.add(b.getBox(x, y + 1).get());
            } 
            /*  check if there is a piece of the opponent in the 
             *  box north-east of the piece
             */
            if ((this.pu.checkRange(x + 1, y + 1))
                   && (!b.getBox(x + 1, y + 1).get().isEmpty())
                        && (b.getBox(x + 1, y + 1).get().getPiece().get().getColour() != colour)) {
                            moves.add(b.getBox(x + 1, y + 1).get());
            }
            /*  check if there is a piece of the opponent in the 
             *  box north-west of the piece
             */ 
            if ((this.pu.checkRange(x - 1, y + 1))
                && (!b.getBox(x - 1, y + 1).get().isEmpty())
                    && (b.getBox(x - 1, y + 1).get().getPiece().get().getColour() != colour)) {
                        moves.add(b.getBox(x - 1, y + 1).get());
            }
        } else {
            if ((isFirstMove)
                && (b.getBox(x, y - 2).get().isEmpty() 
                        && b.getBox(x, y - 1).get().isEmpty())) {
                            moves.add(b.getBox(x, y - 2).get());
            }

            if ((this.pu.checkRange(x, y - 1))
                 && (b.getBox(x, y - 1).get().isEmpty())) {
                     moves.add(b.getBox(x, y - 1).get());
            }
            /*  check if there is a piece of the opponent in the 
             *  box south-east of the piece
             */
            if ((this.pu.checkRange(x + 1, y - 1))
                && (!b.getBox(x + 1, y - 1).get().isEmpty())
                    && (b.getBox(x + 1, y - 1).get().getPiece().get().getColour() != colour)) {
                        moves.add(b.getBox(x + 1, y - 1).get());
            }
            /*  check if there is a piece of the opponent in the 
             *  box south-west of the piece
             */ 
            if ((this.pu.checkRange(x - 1, y - 1))
                && (!b.getBox(x - 1, y - 1).get().isEmpty())
                    && (b.getBox(x - 1, y - 1).get().getPiece().get().getColour() != colour)) {
                        moves.add(b.getBox(x - 1, y - 1).get());
            }
        }
        this.pu.checkBoxMoves(colour, moves, b);
        return moves;
    }

    @Override
    public final String getName() {
        return NAME;
    }
}
