package boardgames.model.piece;

import java.util.List;
import java.util.Optional;
import boardgames.model.board.BoardImpl;
import boardgames.model.board.Box;
import boardgames.utility.Colour;

/**
 * This interface defines the common model of the pieces.
 */
public interface Piece {

    /**
     * Set the type of Piece.
     * @param type type of Piece
     */
    void setPieceType(PieceType type);

    /**
     * @return the type of the Piece if it was initialized, otherwise an empty optional
     */
    Optional<PieceType> getPieceType();

    /**
     * 
     * @return the String representation of the Piece: Box and Colour
     */
    String toString();

    /**
     * @return an Optional containing the Box of the Piece, otherwise an empty optional
     */
    Optional<Box> getBox();

    /**
     * Set the new Piece position.
     * @param piecePosition Box indicating the new position of the piece
     */
    void setBox(Optional<Box> piecePosition);

    /**
     * @return the Colour of the Piece
     */
    Colour getColour();

    /**
     * @return the String representation of the Piece if Piecetype is setted, otherwise throws a IllegalStateException
     */
    String getName();

    /**
     * @param b 
     * @return a List containing the possible Box where the Piece can move
     */
    List<Box> possibleMoves(BoardImpl b);
}

