package boardgames.model.piece;

import java.util.LinkedList;
import java.util.List;
import boardgames.model.board.BoardImpl;
import boardgames.model.board.Box;
import boardgames.utility.Colour;
import boardgames.utility.PieceUtilsImpl;

/**
 * This class implements the functions defined by PieceType for representing the Rook's logic.
 */
public class Rook implements PieceType {

    private final PieceUtilsImpl pu = new PieceUtilsImpl();
    private static final  String NAME = "Rook";

    @Override
    public final List<Box> possibleMoves(final BoardImpl b, final Box current, final Colour colour) {
        final int x = current.getX();
        final int y = current.getY();
        final List<Box> moves = new LinkedList<>();

        /*movimento nord*/
        for (int i = y + 1; i <= PieceUtilsImpl.BOARD_SIZE; i++) {
                /*controllo se il box sopra è occupato*/
                if (!b.getBox(x, i).get().isEmpty()) {
                    /*se il box è occupato da un pezzo avversario posso mangiarlo*/
                    if (b.getBox(x, i).get().getPiece().get().getColour() != colour) {
                        moves.add(b.getBox(x, i).get());
                        //non posso scavalcare i pezzi quindi esco dal ciclo for
                        i = PieceUtilsImpl.BOARD_SIZE;
                    } else {
                        i = PieceUtilsImpl.BOARD_SIZE;
                    }
                } else {
                    /*il box è libero e quindi posso andarci sopra*/
                    moves.add(b.getBox(x, i).get());
                }
        }
        /*movimento sud*/
        for (int i = y - 1; i >= 0; i--) {
                /*controllo se pezzo sopra è occupato*/
                if (!b.getBox(x, i).get().isEmpty()) {
                    /*se il box è occupato da un pezzo avversario posso mangiarlo*/
                    if (b.getBox(x, i).get().getPiece().get().getColour() != colour) {
                        moves.add(b.getBox(x, i).get());
                    //non posso scavalcare i pezzi quindi esco dal ciclo for
                        i = 0;
                    } else {
                        i = 0;
                    }
                } else {
                    /*il box è libero e quindi posso andarci sopra*/
                    moves.add(b.getBox(x, i).get());
                }
        }
        /*movimento est*/
        for (int i = x + 1; i <= PieceUtilsImpl.BOARD_SIZE; i++) {
                /*controllo se pezzo sopra è occupato*/
                if (!b.getBox(i, y).get().isEmpty()) {
                    /*se il box è occupato da un pezzo avversario posso mangiarlo*/
                    if (b.getBox(i, y).get().getPiece().get().getColour() != colour) {
                        moves.add(b.getBox(i, y).get());
                        //non posso scavalcare i pezzi quindi esco dal ciclo for
                        i = PieceUtilsImpl.BOARD_SIZE;
                    } else {
                        i = PieceUtilsImpl.BOARD_SIZE;
                    }
                } else {
                    /*il box è libero e quindi posso andarci sopra*/
                    moves.add(b.getBox(i, y).get());
                }
        }
        /*movimento ovest*/
        for (int i = x - 1; i >= 0; i--) {
                /*controllo se pezzo sopra è occupato*/
                if (!b.getBox(i, y).get().isEmpty()) {
                    /*se il box è occupato da un pezzo avversario posso mangiarlo*/
                    if (b.getBox(i, y).get().getPiece().get().getColour() != colour) {
                        moves.add(b.getBox(i, y).get());
                    //non posso scavalcare i pezzi quindi esco dal ciclo for
                        i = 0;
                    } else {
                        i = 0;
                    }
                } else {
                    /*il box è libero e quindi posso andarci sopra*/
                    moves.add(b.getBox(i, y).get());
                }
        }
        this.pu.checkRangeMoves(moves);
        this.pu.checkBoxMoves(colour, moves, b);
        return moves;
    }

    @Override
    public final String getName() {
        return NAME;
    }
}
