package boardgames.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

import boardgames.model.CheckersGame;
import boardgames.model.board.BoardImpl;
import boardgames.model.board.Box;
import boardgames.model.board.CheckersBoard;
import boardgames.model.piece.CheckersPawn;
import boardgames.model.piece.PieceImpl;
import boardgames.utility.Colour;
import boardgames.utility.PairImpl;

/**
 * Test class for Checkers methods.
 *
 */
public class CheckersLogicTest {
    private CheckersGame game;
    private BoardImpl b;

    /**
     * Initialize Checkers game.
     */
    @Before
    public void setGame() {
        game = new CheckersGame();
        b = new BoardImpl();
        b.setBoardType(new CheckersBoard());
        b.reset();
    }

    /**
     * Black and white player at the beginning have 12 pieces each.
     */
    @Test
    public void initialSize() {
     // CHECKSTYLE: MagicNumber OFF
        assertEquals(game.getBoard().getBlackPieces().size(), 12, "Assert size is 12");
        assertEquals(game.getBoard().getWhitePieces().size(), 12, "Assert size is 12");
    }

    /**
     * Testing pieces initial possible moves.
     */
    @Test
    public void chessPiecesInitialPossibleMovesTest() {
        assertTrue("Pawn 0 no moves", game.getBoard().getWhitePieces().get(0).possibleMoves(b).isEmpty());
        assertTrue("Pawn 2 no moves", game.getBoard().getWhitePieces().get(2).possibleMoves(b).isEmpty());
        assertTrue("Pawn 4 no moves", game.getBoard().getWhitePieces().get(4).possibleMoves(b).isEmpty());
        assertTrue("Pawn 6 no moves", game.getBoard().getWhitePieces().get(6).possibleMoves(b).isEmpty());
        assertTrue("Pawn 8 no moves", game.getBoard().getWhitePieces().get(8).possibleMoves(b).isEmpty());
        assertTrue("Pawn 9 no moves", game.getBoard().getWhitePieces().get(9).possibleMoves(b).isEmpty());
        assertTrue("Pawn 10 no moves", game.getBoard().getWhitePieces().get(10).possibleMoves(b).isEmpty());
        assertTrue("Pawn 11 no moves", game.getBoard().getWhitePieces().get(11).possibleMoves(b).isEmpty());
        assertEquals(game.getBoard().getWhitePieces().get(1).possibleMoves(b).size(), 2, "Assert Pawn have 2 moves");
        assertEquals(game.getBoard().getWhitePieces().get(3).possibleMoves(b).size(), 2, "Assert Pawn have 2 moves");
        assertEquals(game.getBoard().getWhitePieces().get(5).possibleMoves(b).size(), 2, "Assert Pawn have 2 moves");
        assertEquals(game.getBoard().getWhitePieces().get(7).possibleMoves(b).size(), 1, "Assert Pawn have 1 moves");
    }

    /**
     * Test piece possible moves after movement.
     */
    @Test
    public void chessPiecesAfterMovementPossibleMovesTest() {
        assertTrue("Pawn 7 can move in 6,3", game.move(Colour.White, game.getBoard().getWhitePieces().get(7), b.getBox(6, 3)));
        game.updateMove(game.getBoard().getWhitePieces().get(7), b.getBox(6, 3));
        assertTrue("Pawn 11 now can move in 7,2", game.move(Colour.White, game.getBoard().getWhitePieces().get(11), b.getBox(7, 2)));
        game.updateMove(game.getBoard().getWhitePieces().get(11), b.getBox(7, 2));
        assertFalse("Pawn 9 can't move in 1,2", game.move(Colour.White, game.getBoard().getWhitePieces().get(9), b.getBox(7, 2)));
        assertTrue("Pawn 1 can move in 2,3", game.move(Colour.White, game.getBoard().getWhitePieces().get(1), b.getBox(2, 3)));
        game.updateMove(game.getBoard().getWhitePieces().get(1), b.getBox(2, 3));
        assertTrue("Pawn 9 can now move in 1,2", game.move(Colour.White, game.getBoard().getWhitePieces().get(9), b.getBox(1, 2)));
        game.updateMove(game.getBoard().getWhitePieces().get(9), b.getBox(1, 2));
    }

    /**
     * Test if piece promotion happens in right way.
     */
    @Test
    public void testPromotion() {
        final Box b = new Box(new PairImpl<>(7, 7), Optional.empty());
        final PieceImpl p = new PieceImpl(b, Colour.White);
        p.setPieceType(new CheckersPawn());
        b.setPiece(Optional.of(p));

        game.setLastPieceMoved(Optional.of(p));
        game.promotion(Colour.White, "King");

        assertTrue("Now there is Dama", p.getPieceType().get().getName().equals("King"));

    }

    /**
     * Test if piece is correctly remove from board after it has been eaten.
     */
    @Test
    public void eatTest() {
        b.reset();

        assertTrue("Pawn 1 can move", game.move(Colour.White, game.getBoard().getWhitePieces().get(1), game.getBoard().getBox(2, 3)));
        game.updateMove(game.getBoard().getWhitePieces().get(1), game.getBoard().getBox(2, 3));

        assertTrue("Pawn 9 can move", game.move(Colour.Black, game.getBoard().getBlackPieces().get(9), game.getBoard().getBox(3, 4)));
        game.updateMove(game.getBoard().getBlackPieces().get(9), game.getBoard().getBox(3, 4));

        assertTrue("Pawn 1 eats", game.move(Colour.White, game.getBoard().getBox(2, 3).get().getPiece().get(), game.getBoard().getBox(4, 5)));
        game.updateMove(game.getBoard().getWhitePieces().get(1), game.getBoard().getBox(4, 5));

        assertFalse("Pawn 9 is no longer on board", game.getBoard().getBlackPieces().contains(game.getLastPieceEated().get(0)));

    }

}
