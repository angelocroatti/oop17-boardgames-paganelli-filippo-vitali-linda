package boardgames.utility; 


/**
 * This enumeration defines the type all board games.
 */
public enum BoardGame {

    /**
     * games.
     */
    CHECKERS, CHESS;
}
