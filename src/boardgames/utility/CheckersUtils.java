package boardgames.utility;

import java.util.List;
import java.util.Map;
import boardgames.model.board.BoardImpl;
import boardgames.model.board.Box;
import boardgames.model.piece.PieceImpl;

/**
 * This interface provides useful operations to checkers logic.
 */
public interface CheckersUtils {

    /**
     * Inserts in moves the possible moves of the calling white piece.
     * @param b the chessboard on which you are playing
     * @param current the Box that indicate the current position of the calling piece
     * @param moves the List that will contain the possible moves
     */
    void whiteMoves(BoardImpl b, Box current, List<Box> moves);

    /**
     * Inserts in moves the possible moves of the calling black piece.
     * @param b the chessboard on which you are playing
     * @param current the Box that indicate the current position of the calling piece
     * @param moves the List that will contain the possible moves
     */
    void blackMoves(BoardImpl b, Box current, List<Box> moves);

    /**
     * Sets all possible moves (where an opponent's piece is eaten) that the calling white piece can perform.
     * @param b the chessboard on which you are playing
     * @param current the Box that indicate the current position of the calling piece
     * @param colour the colour of the calling piece
     */
    void calcWhiteEatMoves(BoardImpl b, Box current, Colour colour);

    /**
     * Sets all possible moves (where an opponent's piece is eaten) that the calling black piece can perform.
     * @param b the chessboard on which you are playing
     * @param current the Box that indicate the current position of the calling piece
     * @param colour the colour of the calling piece
     */
    void calcBlackEatMoves(BoardImpl b, Box current, Colour colour);

    /**
     * @return the set of possible moves where it is possible to eat an opposing piece 
     * that the calling piece can perform at a given moment
     */
    Map<Box, List<PieceImpl>> getPaths();

    /**
     * @param newPosition the Box that indicated where the calling piece will be moved
     * @return a List containing the pieces that are eaten when making the indicated move
     */
    List<PieceImpl> getPossibleEated(Box newPosition);
}
