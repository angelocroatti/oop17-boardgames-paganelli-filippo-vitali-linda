package boardgames.utility;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import boardgames.model.board.BoardImpl;
import boardgames.model.board.Box;
import boardgames.model.piece.PieceImpl;

/**
 * This class provides useful operations to checkers logic by implementing the declarations of the CheckersUtils interface.
 */
public class CheckersUtilsImpl implements CheckersUtils {

    private final PieceUtilsImpl cp;
    private final Map<Box, List<PieceImpl>> possiblePath;

    /**
     * Initializes the components useful for managing the checker logic:
     * - PieceUtilsImpl provides the basic operations useful for all the pieces.
     * - possiblePath contains all possible moves combinations of the piece at a given time.
     */
    public CheckersUtilsImpl() {
        this.cp = new PieceUtilsImpl();
        this.possiblePath = new HashMap<Box, List<PieceImpl>>();
    }

    @Override
    public final void whiteMoves(final BoardImpl b, final Box current, final List<Box> moves) {
        final int x = current.getX();
        final int y = current.getY();

        //nord-est
        if (this.cp.checkRange(x + 1, y + 1) && b.getBox(x + 1, y + 1).get().isEmpty()) {
            moves.add(b.getBox(x + 1, y + 1).get());
        }
        //nord-ovest
        if (this.cp.checkRange(x - 1, y + 1) && b.getBox(x - 1, y + 1).get().isEmpty()) {
            moves.add(b.getBox(x - 1, y + 1).get());
        }
    }

    @Override
    public final void blackMoves(final BoardImpl b, final Box current, final List<Box> moves) {
        final int x = current.getX();
        final int y = current.getY();

        //sud-est
        if (this.cp.checkRange(x + 1, y - 1) && b.getBox(x + 1, y - 1).get().isEmpty()) {
            moves.add(b.getBox(x + 1, y - 1).get());
        }
        //sud-ovest
        if (this.cp.checkRange(x - 1, y - 1) && b.getBox(x - 1, y - 1).get().isEmpty()) {
            moves.add(b.getBox(x - 1, y - 1).get());
        }
    }

    @Override
    public final void calcWhiteEatMoves(final BoardImpl b, final Box current, final Colour colour) {
        final int x = current.getX();
        final int y = current.getY();

        //nord-est
        //se è presente un pezzo avversario e il box dopo è vuoto, aggiungo pezzo mangiato
        if (this.cp.checkRange(x + 1, y + 1) && (!b.getBox(x + 1, y + 1).get().isEmpty())
                && (b.getBox(x + 1, y + 1).get().getPiece().get().getColour() != colour)
                    && this.cp.checkRange(x + 2, y + 2) && (b.getBox(x + 2, y + 2).get().isEmpty())) {
                        final List<PieceImpl> eating = new LinkedList<>();
                        if (this.possiblePath.containsKey(current)) {
                            // se la chiave è contenuta ed è possibile mangiare ancora significa che esiste un cammino più lungo
                            // quindi aggiungo anche i pezzi mangiati nel cammino precedente
                            eating.addAll(this.possiblePath.get(current));
                        }
                        eating.add(b.getBox(x + 1, y + 1).get().getPiece().get());
                        this.possiblePath.put(b.getBox(x + 2, y + 2).get(), eating);
                        this.calcWhiteEatMoves(b, b.getBox(x + 2, y + 2).get(), colour);
        }
        //nord-ovest
        //se è presente un pezzo avversario e il box dopo è vuoto, aggiungo pezzo mangiato
        if (this.cp.checkRange(x - 1, y + 1) && (!b.getBox(x - 1, y + 1).get().isEmpty())
                && (b.getBox(x - 1, y + 1).get().getPiece().get().getColour() != colour)
                    && this.cp.checkRange(x - 2, y + 2) && (b.getBox(x - 2, y + 2).get().isEmpty())) {
                        final List<PieceImpl> eating = new LinkedList<>();
                        if (this.possiblePath.containsKey(current)) {
                            // se la chiave è contenuta ed è possibile mangiare ancora significa che esiste un cammino più lungo
                            // quindi aggiungo anche i pezzi mangiati nel cammino precedente
                            eating.addAll(this.possiblePath.get(current));
                        }
                        eating.add(b.getBox(x - 1, y + 1).get().getPiece().get());
                        this.possiblePath.put(b.getBox(x - 2, y + 2).get(), eating);
                        this.calcWhiteEatMoves(b, b.getBox(x - 2, y + 2).get(), colour);
        }
    }

    @Override
    public final void calcBlackEatMoves(final BoardImpl b, final Box current, final Colour colour) {
        final int x = current.getX();
        final int y = current.getY();

        //sud-est
        //se è presente un pezzo avversario e il box dopo è vuoto, aggiungo pezzo mangiato
        if (this.cp.checkRange(x + 1, y - 1) && (!b.getBox(x + 1, y - 1).get().isEmpty()) 
                && (b.getBox(x + 1, y - 1).get().getPiece().get().getColour() != colour)
                    && (this.cp.checkRange(x + 2, y - 2) && (b.getBox(x + 2, y - 2).get().isEmpty()))) {
                        final List<PieceImpl> eating = new LinkedList<>();
                        if (this.possiblePath.containsKey(current)) {
                            // se la chiave è contenuta ed è possibile mangiare ancora significa che esiste un cammino più lungo
                            // quindi aggiungo anche i pezzi mangiati nel cammino precedente
                            eating.addAll(this.possiblePath.get(current));
                        }
                        eating.add(b.getBox(x + 1, y - 1).get().getPiece().get());
                        this.possiblePath.put(b.getBox(x + 2, y - 2).get(), eating);
                        this.calcBlackEatMoves(b, b.getBox(x + 2, y - 2).get(), colour);
        }

        //sud-ovest
        //se è presente un pezzo avversario e il box dopo è vuoto, aggiungo pezzo mangiato
        if (this.cp.checkRange(x - 1, y - 1) && (!b.getBox(x - 1, y - 1).get().isEmpty())
                && (b.getBox(x - 1, y - 1).get().getPiece().get().getColour() != colour) 
                    && (this.cp.checkRange(x - 2, y - 2) && (b.getBox(x - 2, y - 2).get().isEmpty()))) {
                        final List<PieceImpl> eating = new LinkedList<>();
                        if (this.possiblePath.containsKey(current)) {
                        // se la chiave è contenuta ed è possibile mangiare ancora significa che esiste un cammino più lungo
                        // quindi aggiungo anche i pezzi mangiati nel cammino precedente
                            eating.addAll(this.possiblePath.get(current));
                        }
                        eating.add(b.getBox(x - 1, y - 1).get().getPiece().get());
                        this.possiblePath.put(b.getBox(x - 2, y - 2).get(), eating);
                        this.calcBlackEatMoves(b, b.getBox(x - 2, y - 2).get(), colour);
        }
    }

    @Override
    public final Map<Box, List<PieceImpl>> getPaths() {
        return this.possiblePath;
    }

    @Override
    public final List<PieceImpl> getPossibleEated(final Box newPosition) {
        if (this.possiblePath.containsKey(newPosition)) {
            return this.possiblePath.get(newPosition);
        } else {
            return Collections.emptyList();
        }
    }
}
