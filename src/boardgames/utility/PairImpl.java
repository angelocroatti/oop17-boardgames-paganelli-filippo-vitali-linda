package boardgames.utility;

/**
 * This class implements the functions defined by PairImpl for modeling a 2D point.
 * @param <X> the x coordinate of the Pair
 * @param <Y> the y coordinate of the Pair
 */
public class PairImpl<X, Y> implements Pair<X, Y> {
    private final X x;
    private final Y y;

    /**
     * Initializes the coordinates of the Pair.
     * @param x the x coordinate of the Pair
     * @param y the y coordinate of the Pair
     */
    public PairImpl(final X x, final Y y) {
        super();
        this.x = x;
        this.y = y;
    }

    @Override
    public final X getX() {
        return x;
    }

    @Override
    public final Y getY() {
        return y;
    }

    @Override
    public final int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((x == null) ? 0 : x.hashCode());
        result = prime * result + ((y == null) ? 0 : y.hashCode());
        return result;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PairImpl other = (PairImpl) obj;
        if (x == null) {
            if (other.x != null) {
                return false;
            }
        } else if (!x.equals(other.x)) {
            return false;
        }
        if (y == null) {
            if (other.y != null) {
                return false;
            }
        } else if (!y.equals(other.y)) {
            return false;
        }
        return true;
    }

    @Override
    public final String toString() {
        return "[x = " + x + ", y = " + y + "]";
    }
}
