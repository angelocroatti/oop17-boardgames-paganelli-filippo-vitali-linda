package boardgames.view.board;

import java.awt.event.ActionListener;
import java.util.Map;
import java.util.Optional;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import boardgames.model.board.Box;

/**
 * Interface for a generic game board that can be used by every board games.
 *
 */
public interface BoardView {

    /**
     * Method that provide the game board's view.
     * 
     * @return map that contains every buttons with theri pieces of the board
     */
    Map<JButton, Optional<Box>> getButtons();

    /**
     * Method that provide the JPanel initialized of chess o checkers board.
     * 
     * @return JPanel
     */
    JPanel getGridPanel();

    /**
     * This methods sets the JButtons in the grid (with their colors) and the axis,
     * used to define the coordinates for each button.
     * 
     *
     * @param al
     *            action listener that provides the action for all buttons
     */
    void initBoardGrid(ActionListener al);

    /**
     * Method that repaint the board's color after every move.
     */
    void repaintBoard();

    /**
     * Place the images corresponding to the pieces on the board.
     */
    void arrangeInitPiecesIcon();

    /**
     * method that shrinks (or enlarges) the icon of the board's pieces, according
     * to buttons dimension.
     * 
     * @param color
     *            of piece to take the picture of
     * @param name
     *            of piece to take the picture of
     * @param perc
     *            rate of picture resizing
     * @return new ImageIcon resized
     */
    ImageIcon resizeIcon(String color, String name, double perc);

}
