package boardgames.view.board;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.IntStream;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import javax.swing.JPanel;

import boardgames.controller.GameControllerImpl;
import boardgames.model.board.Box;
import boardgames.view.utility.GameFrameFactory;
import boardgames.view.utility.MainFrameFactory;

/**
 * Class that provides methods to initialize and draw the game board.
 *
 */
public abstract class BoardViewImpl implements BoardView {

    private static final Integer RGB1 = 11435303;
    private static final Integer RGB2 = 13483421;
    private final Integer numButtons;
    private static final double PERC_RESIZE_ICON = 0.8;
    private final JPanel gridPanel;
    private final GameFrameFactory factory;
    private final Map<JButton, Optional<Box>> buttons;
    private final GameControllerImpl controller;

    /**
     * Constructor of the class, that creates the frame of board.
     * 
     * @param controller
     *            of entire application
     *@param dim board dimension
     */
    public BoardViewImpl(final GameControllerImpl controller, final Integer dim) {
        this.numButtons = dim;
        this.factory = new MainFrameFactory();
        this.gridPanel = factory.cratePanel(Optional.empty(), new GridLayout(numButtons, numButtons), true);
        this.buttons = new HashMap<>();
        this.controller = controller;

    }

    @Override
    public final Map<JButton, Optional<Box>> getButtons() {
        return buttons;
    }

    @Override
    public final JPanel getGridPanel() {
        return gridPanel;
    }

    @Override
    public final void initBoardGrid(final ActionListener al) {
        IntStream.range(0, numButtons).boxed().sorted(Comparator.reverseOrder())
                .forEach(i -> IntStream.range(0, numButtons).forEach(j -> {
                    final JButton jb = factory.createButton(Optional.of(al), Optional.empty());
                    this.setBoxesBackground(j, i, jb);
                    this.buttons.put(jb, this.controller.getGameBoard().getBox(j, i));
                    jb.setBorderPainted(false);
                    this.gridPanel.add(jb);
                }));
    }

    @Override
    public final void repaintBoard() {
        this.buttons.keySet().stream().filter(j -> j.getBackground().equals(Color.cyan))
                .peek(j -> j.setBorderPainted(false))
                .forEach(jb -> this.setBoxesBackground(buttons.get(jb).get().getX(), buttons.get(jb).get().getY(), jb));

    }

    @Override
    public final void arrangeInitPiecesIcon() {
        controller.getGamePieces()
                .forEach(gamePiece -> this.buttons.keySet().stream()
                        .filter(bj -> this.buttons.get(bj).get().getPair().equals(gamePiece.getBox().get().getPair()))
                        .forEach(bj -> {
                            this.buttons.get(bj).get().setPiece(Optional.of(gamePiece));
                            bj.setIcon(this.resizeIcon(gamePiece.getColour().toString(), gamePiece.getName(),
                                    PERC_RESIZE_ICON));
                        }));
    }

    /**
     * Set the colors of the board that are inverse for checkers and chess.
     * 
     * @param i
     *            first coordinate
     * @param j
     *            second coordinate
     * @param jb
     *            button to color
     */
    private void setBoxesBackground(final int i, final int j, final JButton jb) {
        if ((j % 2 == 0 && i % 2 == 0) || (j % 2 != 0 && i % 2 != 0)) {
            jb.setBackground(this.getFirstBoxColor());
        } else {
            jb.setBackground(this.getSecondBoxColor());
        }
        jb.setOpaque(true); //necessario for mac systems

    }


    @Override
    public final ImageIcon resizeIcon(final String color, final String name, final double perc) {
        final ImageIcon ia = new ImageIcon(
                BoardViewImpl.class.getResource("/images/" + color + name + this.nameIconExtension()));
        final JButton bj = buttons.keySet().stream().findFirst().get();
        final Image resizableImage = ia.getImage();
        return new ImageIcon(resizableImage.getScaledInstance((int) (bj.getSize().getWidth() * perc),
                (int) (bj.getSize().getHeight() * perc), Image.SCALE_SMOOTH));

    }

    /**
     * @return the string piece of the icon name that specifies the name and its
     *         extension
     */
    protected abstract String nameIconExtension();

    /**
     * @return the first color that will be put as a background on the button, since
     *         the checkersboard and chessboard colors are reversed
     */
    protected abstract Color getFirstBoxColor();

    /**
     * @return the second color that will be put as a background on the button,
     *         since the checkerboard and chessboard colors are reversed
     */
    protected abstract Color getSecondBoxColor();

    /**
     * @return darker color of the board's box
     */
    protected static Integer getRgb1() {
        return RGB1;
    }

    /**
     * @return lighter color of the board's box
     */
    protected static Integer getRgb2() {
        return RGB2;
    }

}
