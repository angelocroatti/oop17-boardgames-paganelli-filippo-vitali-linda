package boardgames.view.board;

import java.awt.Color;


import boardgames.controller.GameControllerImpl;

/**
 * This class is used for Chess board's settings.
 *
 */
public class ChessBoardView extends BoardViewImpl {


    /**
     * @param ctr controller of the entire project
     * @param dim board dimension
     */
    public ChessBoardView(final GameControllerImpl ctr, final Integer dim) {
        super(ctr, dim);
    }


    @Override
    protected final Color getFirstBoxColor() {
        return new Color(BoardViewImpl.getRgb1());

    }
    @Override
    protected final Color getSecondBoxColor() {
        return new Color(BoardViewImpl.getRgb2());

    }

    @Override
    protected final String nameIconExtension() {
        return "(Chess).png";
    }

}
