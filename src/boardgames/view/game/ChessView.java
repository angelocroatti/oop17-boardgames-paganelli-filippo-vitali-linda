package boardgames.view.game;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Optional;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import boardgames.controller.GameControllerImpl;
import boardgames.view.board.BoardView;

/**
 * Class that manage the view "action" for Chess game like create promotion
 * panel, set movement and positioning icon on GUI.
 *
 */
public class ChessView extends GameViewImpl {
    private static final Logger LOGGER = LogManager.getLogger(ChessView.class.getName());

    /**
     * @param c controller of entire game
     * @param bv chess board view
     * @param timed true if is a timed match
     */
    public ChessView(final GameControllerImpl c, final BoardView bv, final boolean timed) {
        super(c, bv, timed);
    }

    @Override
    protected final void popUpImplementation() {
        final JFrame frame = getPopFactory().createFrame(Optional.of("King under check"));
        final JPanel panel = getPopFactory().cratePanel(
                Optional.of(GameControllerImpl.getController().getTurn() + " king " + "is under check"),
                new GridBagLayout(), true);
        panel.add(getPopFactory().createButton(Optional.of(e -> frame.dispose()), Optional.of("Ok")));
        frame.add(panel);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }

    @Override
    public final JPanel promotionPanel(final JFrame frame) {
        final JPanel panel = getPopFactory().cratePanel(Optional.of("Please select the piece to replace with"),
                new GridBagLayout(), true);
        final GridBagConstraints gbc = getPopFactory().createGridBagConstraints();
        panel.add(getPopFactory().createButton(Optional.of(e -> this.replaceAction(frame, "Queen")),
                Optional.of("Queen")), gbc);
        gbc.gridy++;
        panel.add(
                getPopFactory().createButton(Optional.of(e -> this.replaceAction(frame, "Rook")), Optional.of("Rook")),
                gbc);
        gbc.gridy++;
        panel.add(getPopFactory().createButton(Optional.of(e -> this.replaceAction(frame, "Bishop")),
                Optional.of("Bishop")), gbc);
        gbc.gridy++;
        panel.add(getPopFactory().createButton(Optional.of(e -> this.replaceAction(frame, "Knight")),
                Optional.of("Knight")), gbc);
        return panel;

    }

    @Override
    public final void setMovement(final JButton b) {
        if (!getBoardView().getButtons().get(b).get().getPiece().equals(Optional.empty())
                && !GameControllerImpl.getController().getPieceToMove().equals(Optional.empty())
                && GameControllerImpl.getController()
                        .getPossibleMoves(GameControllerImpl.getController().getPieceToMove())
                        .contains(getBoardView().getButtons().get(b).get().getPiece().get().getBox().get())) {

            LOGGER.debug(GameControllerImpl.getController().getPieceToMove().get().getColour() + " "
                    + GameControllerImpl.getController().getPieceToMove().get().getName() + " eats "
                    + getBoardView().getButtons().get(b).get().getPiece().get().getColour() + " "
                    + getBoardView().getButtons().get(b).get().getPiece().get().getName() + "\n");

            GameControllerImpl.getController().movePieceTo(getBoardView().getButtons().get(b));

        } else if (!getBoardView().getButtons().get(b).get().getPiece().equals(Optional.empty())) {
            GameControllerImpl.getController().setPieceToMove(getBoardView().getButtons().get(b).get().getPiece());
            this.lightUpPossibleMoves(b);

        } else if (!GameControllerImpl.getController().getPieceToMove().equals(Optional.empty())) {
            GameControllerImpl.getController().movePieceTo(getBoardView().getButtons().get(b));

        }

    }

    @Override
    public final void setIconAfterMove(final JButton arrival, final JButton start) {
        arrival.setIcon(start.getIcon());
        start.setIcon(null);

    }

}
