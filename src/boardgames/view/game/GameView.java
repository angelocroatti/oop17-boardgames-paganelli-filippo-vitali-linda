package boardgames.view.game;

import java.util.Optional;

import javax.swing.JButton;

import boardgames.model.board.Box;

/**
 * Interface for the creation and setting the user interaction of the game's GUI.
 *
 */
public interface GameView {

    /**
     * Method that create and initialize the main GUI component.
     * 
     */
    void drawGraphics();

    /**
     * Method that shows pieces movement on game board view.
     * @param from the place where is the piece to move and where to remove the icon
     * @param to the place to draw piece moved
     */
    void drawMove(Optional<Box> from, Optional<Box> to);



    /**Method that shows to user where the selected piece can be moved.
     * @param jb button that contains the piece of which to show possible moves
     */
    void lightUpPossibleMoves(JButton jb);

    /**
     * Method that draw the eated piece in corresponding color graveyard panel.
     */
    void sendPieceToGraveyard();

    /**
     * Method that shows the final winning panel that when match is finished and closes the game.
     */
    void updateWinStatus();

    /**
     * Method that shows the promotion panel which user can chose the piece to replace with.
     *
     */
    void updatePromotionStatus();



    /**
     * Method that appears if a piece is in a particular condition.
     */
    void updatePieceStatus();


    /**
     * Method that update console match log with play events.
     * @param label that will be shows
     */
    void updateConsole(String label);

}
