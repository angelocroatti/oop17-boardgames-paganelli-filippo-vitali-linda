package boardgames.view.game;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.util.List;
import java.util.Optional;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneLayout;

import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import boardgames.controller.GameControllerImpl;
import boardgames.controller.GameStopwatch;
import boardgames.model.board.Box;
import boardgames.model.piece.PieceImpl;
import boardgames.utility.Colour;
import boardgames.view.board.BoardView;
import boardgames.view.utility.GameFrameFactory;
import boardgames.view.utility.MainFrameFactory;
import boardgames.view.utility.PopUpFactory;

/**
 * This class creates a simple GUI to settle the BoardGames environment. Common
 * features are used for both a Chess and a Checkers match (either with or
 * without time).
 */
public abstract class GameViewImpl implements GameView {

    private static final int CONSOLE_PREF_SIZE = (int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth() * 0.07);

    private static final int GRAVEYARD_PREF_SIZE = (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight()
            * 0.13);
    private static final double BOARD_SIZE = Toolkit.getDefaultToolkit().getScreenSize().getHeight() * 0.6;
    private static final double PERC_RESIZE_GRAVEYARD_ICON = 0.4;
    private static final double PERC_RESIZE_ICON = 0.8;
    private static final String TITLE = "BoardsGames";
    private static final String NEWLINE = "\n";

    private static final String EXIT_BUTTON = "Exit";
    private static final String GRAVEYARD_P1 = "White Graveyard";
    private static final String GRAVEYARD_P2 = "Black Graveyard";

    private static final String STARTGAME_STATEMENT1 = "Welcome to BoardGames! A new match begins, ready?"
            + " White player moves first";

    private final JFrame myFrame;
    private final JPanel graveyardBlack;
    private final JPanel graveyardWhite;
    private final JTextArea console;
    private final JScrollPane consoleContainer;
    private final JPanel centralPanel;
    private final GroupLayout centralPanelLayout;

    private final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    private static final double PERC_RESIZE_MAINFRAME_HEIGHT = 0.8;
    private static final double PERC_RESIZE_MAINFRAME_WIDTH = 0.6;

    private final GameControllerImpl controller;
    private final BoardView boardView;
    private final TimeGameView timeGameView;
    private final GameFrameFactory popFactory;
    private static final Logger LOGGER = LogManager.getLogger(GameViewImpl.class.getName());

    /**
     * Constructor who is responsible for creating the main components of the game frame,
     *  as well as correctly initializing the fields of his class.
     * @param c controller of the game
     * @param bv the actual game (Chess or Checkers)
     * @param timed boolean that is true if the user has selected "play with time"
     */
    public GameViewImpl(final GameControllerImpl c, final BoardView bv, final boolean timed) {
        this.controller = c;
        this.boardView = bv;
        this.popFactory = new PopUpFactory();
        this.timeGameView = new TimeGameViewImpl();
        final GameFrameFactory mainFactory = new MainFrameFactory();

        this.myFrame = mainFactory.createFrame(Optional.of(TITLE));
        this.graveyardBlack = mainFactory.cratePanel(Optional.of(GRAVEYARD_P2), new FlowLayout(), true);
        this.graveyardWhite = mainFactory.cratePanel(Optional.of(GRAVEYARD_P1), new FlowLayout(), true);
        this.console = MainFrameFactory.createJTextArea(Optional.of(STARTGAME_STATEMENT1), true, false);
        this.consoleContainer = mainFactory.createJScrollPane(console, new ScrollPaneLayout());
        this.centralPanel = mainFactory.cratePanel(Optional.empty(), new FlowLayout(), true);
        this.centralPanelLayout = mainFactory.createGroupLayout(centralPanel);
        this.centralPanel.setLayout(centralPanelLayout);
        if (timed) {
            timeGameView.setTimerPanelVisible();
            this.console.append("This is a timed match. You have 15 minutes each, win by checkmate or by eating more pieces then "
                    + NEWLINE + "your opponent, before the time is up !" + NEWLINE);
        }

    }

    @Override
    public final void drawGraphics() {
        this.boardView.initBoardGrid(e -> setMovement((JButton) e.getSource()));

        centralPanelLayout.setHorizontalGroup(centralPanelLayout.createSequentialGroup()
                .addGroup(centralPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(graveyardWhite, 0, GRAVEYARD_PREF_SIZE, Short.MAX_VALUE)
                        .addComponent(timeGameView.getPlayPanel(), 0, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
                        .addComponent(timeGameView.getWhitePanel(), 0, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE))
                .addGroup(centralPanelLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
                        .addComponent(boardView.getGridPanel(), 0, (int) BOARD_SIZE, Short.MAX_VALUE)
                        .addComponent(consoleContainer, 0, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE))
                .addGroup(centralPanelLayout.createSequentialGroup())
                .addGroup(centralPanelLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
                        .addComponent(graveyardBlack, 0, GRAVEYARD_PREF_SIZE, Short.MAX_VALUE)
                        .addComponent(timeGameView.getBlackPanel(), 0, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)));

        centralPanelLayout.setVerticalGroup(centralPanelLayout.createSequentialGroup()
                .addGroup(centralPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(graveyardWhite, 0, GRAVEYARD_PREF_SIZE, Short.MAX_VALUE)
                        .addComponent(boardView.getGridPanel(), 0, (int) BOARD_SIZE, Short.MAX_VALUE)
                        .addComponent(graveyardBlack, 0, GRAVEYARD_PREF_SIZE, Short.MAX_VALUE))
                .addGroup(centralPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addGroup(centralPanelLayout.createSequentialGroup()
                                .addGroup(centralPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(consoleContainer, 0, CONSOLE_PREF_SIZE, Short.MAX_VALUE)))
                        .addComponent(timeGameView.getPlayPanel(), 0, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
                        .addComponent(timeGameView.getWhitePanel(), 0, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
                        .addComponent(timeGameView.getBlackPanel(), 0, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)));

        this.myFrame.add(centralPanel, BorderLayout.CENTER);
        this.myFrame.pack();
        this.myFrame.setVisible(true);
        myFrame.setSize((int) (this.screenSize.getWidth() * PERC_RESIZE_MAINFRAME_WIDTH),
                (int) (this.screenSize.getHeight() * PERC_RESIZE_MAINFRAME_HEIGHT));
        this.myFrame.setLocationRelativeTo(null); /* Sets the frame at the center of desktop */

        this.boardView.arrangeInitPiecesIcon();

    }


    @Override
    public final void drawMove(final Optional<Box> from, final Optional<Box> to) {
        console.append(controller.getPieceToMove().get().getName() + " moves from: " + from.get().getPair() + " to: " + to.get().getPair() + NEWLINE);

        boardView.getButtons().keySet().stream()
                .filter(arrivalButton -> boardView.getButtons().get(arrivalButton).equals(to))
                .forEach(arrivalButton -> boardView.getButtons().keySet().stream()
                        .filter(startButton -> boardView.getButtons().get(startButton).equals(from))
                        .forEach(startButton -> {
                            this.setIconAfterMove(arrivalButton, startButton); 
                        })); 



        this.boardView.repaintBoard();

        console.append("is now " + controller.getTurn() + " turn" + NEWLINE);
    }

    /**
     * Method that draw pieces movement.
     * @param arrival the button where the icon will be set
     * @param start the button where the icon will be take
     */
    protected abstract void setIconAfterMove(JButton arrival, JButton start);

    /**
     * Method that specifics what is the action that will be done after every click on board.
     * @param b the JButton that will be link with action
     */
    protected abstract void setMovement(JButton b);

    /**
     * @return the console of the game GUI
     */
    protected JTextArea getConsole() {
        return console;
    }

    /**
     * @return the board view of the game
     */
    protected BoardView getBoardView() {
        return boardView;
    }

    /**
     * @return the pop up factory that can be used by subclasses
     */
    protected GameFrameFactory getPopFactory() {
        return popFactory;
    }


    @Override
    public final void lightUpPossibleMoves(final JButton jb) {
        final List<Box> moves = controller.getPossibleMoves(boardView.getButtons().get(jb).get().getPiece());
        this.boardView.repaintBoard();
        boardView.getButtons().keySet().stream()
                .filter(button -> moves.stream()
                        .anyMatch(box -> box.getPair().equals(boardView.getButtons().get(button).get().getPair())))
                .forEach(j -> {
                    j.setBackground(Color.cyan);
                    j.setBorderPainted(true);
                });
    }

 

    @Override
    public final void sendPieceToGraveyard() {
        LOGGER.debug("sendPieceToGraveYard " + controller.getEatedPiece());

        if (!controller.getEatedPiece().isEmpty()) {
            controller.getEatedPiece().forEach(p -> {
                console.append(p.getColour() + " " + p.getName() + " was eated" + "\n");
            });
        }
        controller.getEatedPiece().forEach(piece -> {
            final ImageIcon im = boardView.resizeIcon(piece.getColour().toString(), piece.getName(),
                    PERC_RESIZE_GRAVEYARD_ICON);

            if (controller.getTurn().equals(Colour.Black)) {
                graveyardWhite.add(new JLabel(im));

            } else {
                graveyardBlack.add(new JLabel(im));

            }

        });

    }

    @Override
    public final void updatePieceStatus() {
        this.popUpImplementation();
    }

    /**
     * Method used by subclasses to implement appropriate pop up relative
     * to particular piece status.
     */
    protected abstract void popUpImplementation();

    @Override
    public final void updateWinStatus() {
        final JFrame frame = popFactory.createFrame(Optional.of("The match is over"));
        final JPanel panel = popFactory.cratePanel(Optional.of("Winning panel"), new GridBagLayout(), true);
        final GridBagConstraints gbc = popFactory.createGridBagConstraints();
        panel.add(popFactory.createJLabel(Optional.of(controller.getOppositePlayerColour() + " player won")), gbc);
        gbc.gridy++;
        panel.add(popFactory.createButton(Optional.of(e -> System.exit(0)), Optional.of(EXIT_BUTTON)), gbc);
        if (!controller.isTimed()) {
            GameStopwatch.stopPlayTimer();
            LOGGER.trace("Play time: " + DurationFormatUtils.formatDuration(GameStopwatch.getPlayTime().getTime(), "HH:mm:ss"));
        } else {
            if (!GameStopwatch.getBstopwatch().isSuspended()) {
                GameStopwatch.getBstopwatch().stop();
            }
            if (!GameStopwatch.getWstopwatch().isSuspended()) {
                GameStopwatch.getWstopwatch().stop();
            }
        }
        frame.add(panel);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    @Override
    public final void updatePromotionStatus() {
        final JFrame frame = popFactory.createFrame(Optional.of("Promotion pop-up"));
        frame.add(this.promotionPanel(frame));
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }

    /** Method to implement for establish the promotion criteria.
     * @param frame where will be added appropriate panel
     * @return the jpanel containing the JButtons of choices
     */
    protected abstract JPanel promotionPanel(JFrame frame);

    /**Method that make the promotion by calling the controller and the method to change the icon.
     * @param frame where will be added appropriate panel
     * @param name of piece that will be added on the board 
     */
    protected void replaceAction(final JFrame frame, final String name) {
        controller.doPiecePromotion(name);
        this.replacePromotedPieceIcon(controller.pieceMovedTo().get().getPiece(), name);
        frame.dispose();
    }

    private void replacePromotedPieceIcon(final Optional<PieceImpl> piece, final String name) {
        boardView.getButtons().keySet().stream()
                .filter(jb -> boardView.getButtons().get(jb).equals(controller.pieceMovedTo())).findFirst().get()
                .setIcon(boardView.resizeIcon(piece.get().getColour().toString(), name, PERC_RESIZE_ICON)); 
    }

    @Override
    public final void updateConsole(final String label) {
        console.append(label + NEWLINE);
    }

}
