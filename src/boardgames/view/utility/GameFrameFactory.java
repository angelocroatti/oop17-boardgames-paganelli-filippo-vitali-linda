package boardgames.view.utility;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.LayoutManager;
import java.awt.event.ActionListener;
import java.util.Optional;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneLayout;

/**
 * Interface that models a frame factory.
 *
 */
public interface GameFrameFactory {

    /**
     * Method that creates a new JFrame.
     * @param title
     *            the name of frame
     * @return a new Frame with specified dimensions
     */
    JFrame createFrame(Optional<String> title);


    /**
     * Method that creates a new JPanel.
     * @param title the name of panel
     * @param lm the layout to add 
     * @param visible if panel should be visible or not
     * @return new Panel with specified components
     */
    JPanel cratePanel(Optional<String> title, LayoutManager lm, boolean visible);

    /**
     * Method that creates a new JButton.
     * @param al if there is one, the ActionListener that will be associated to the button
     * @param label optional that contains the string that will be set in the button
     * @return new JButton with specified text and actionListener added
     */
    JButton createButton(Optional<ActionListener> al, Optional<String> label);

    /**
     * Method that creates a new JLabel.
     * @param s optional that contains the string that will be set in the Jlabel
     * @return a new JLabel with the specified look
     */
    JLabel createJLabel(Optional<String> s);

    /**
     * Method that creates a new GridBagConstraints.
     * @return new GridBagConstraints that have specific initialization
     */
    GridBagConstraints createGridBagConstraints();

    /**
     * Method that creates a new JCheckBox.
     * @param of 
     *            string that appear in checkbox
     * @return a new CheckBox with name
     */
    JCheckBox createCheckBox(Optional<String> of);

    /**
     * Method that creates a new JScrollPanel.
     * @param c
     *            the component (panel for example) that will be add to JScrollPane
     * @param l
     *            the layout to add
     * @return a new JScrollPanel with specific features
     */
    JScrollPane createJScrollPane(Component c, ScrollPaneLayout l);

    /**
     * Method that creates a new GroupLayout.
     * @param centralPanel
     *            that will be added
     * @return new GroupLayout with specific features
     */
    GroupLayout createGroupLayout(JPanel centralPanel);

}
