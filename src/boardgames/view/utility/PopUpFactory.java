package boardgames.view.utility;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.awt.event.ActionListener;

import java.util.Optional;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneLayout;
import javax.swing.WindowConstants;
import javax.swing.border.TitledBorder;

/**
 * Class that implement a factory for the pop-up frames of the application.
 *
 */
public final class PopUpFactory implements GameFrameFactory {

    private final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

    private static final double PERC_RESIZE_POPUP = 0.2;


    @Override
    public JFrame createFrame(final Optional<String> title) {
        final JFrame jf = new JFrame(title.get());

        jf.setSize((int) (this.screenSize.getWidth() * PERC_RESIZE_POPUP),
                (int) (this.screenSize.getHeight() * PERC_RESIZE_POPUP));
        jf.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        jf.setLayout(new BorderLayout());
        return jf;
    }

    @Override
    public JPanel cratePanel(final Optional<String> question, final LayoutManager lm, final boolean visible) {
        final JPanel jp = new JPanel();
        jp.setLayout(lm);
        if (!question.equals(Optional.empty())) {
            jp.setBorder(new TitledBorder(question.get()));
        }
        jp.setVisible(visible);
        return jp;
    }

    @Override
    public JButton createButton(final Optional<ActionListener> al, final Optional<String> label) {
        final JButton jb = new JButton();
        if (!label.equals(Optional.empty())) {
            jb.setText(label.get());
        }
        if (!al.equals(Optional.empty())) {
            jb.addActionListener(al.get());
        }
        jb.setOpaque(true); //required for mac systems
        return jb;
    }

    @Override
    public JLabel createJLabel(final Optional<String> s) {
        return new JLabel(s.get());
    }

    @Override
    public GridBagConstraints createGridBagConstraints() {
        final GridBagConstraints cnst = new GridBagConstraints();
        cnst.fill = GridBagConstraints.HORIZONTAL;
        cnst.gridy = 0;
        return cnst;
    }

    @Override
    public JCheckBox createCheckBox(final Optional<String> of) {
        return new JCheckBox(of.get());
    }

    @Override
    public JScrollPane createJScrollPane(final Component c, final ScrollPaneLayout l) {
        return new JScrollPane(c);
    }

    @Override
    public GroupLayout createGroupLayout(final JPanel centralPanel) {
        return new GroupLayout(centralPanel);
    }
}
